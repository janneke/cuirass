(use-modules (srfi srfi-1))

(define (local-file file)
  ;; In the common case jobs will be defined relative to the repository.
  ;; However for testing purpose use local gnu-system.scm instead.
  (string-append (dirname (current-filename)) "/" file))

(define (url->file-name url)
  (string-trim
   (string-map (lambda (c) (if (memq c (string->list ":/")) #\- c)) url)
   #\-))

;;.gitconfig:
;;[url "git+ssh://git.sv.gnu.org/srv/git/"]
;;	insteadof = gnu:
;; cd ~/src
;; guix clone gnu:guix
(define vc
  ;; where your version-control checkouts live
  (string-append (getenv "HOME") "/src"))
(define guix-checkout (string-append vc "/guix"))

(define user (assoc-ref
              `(("janneke" . "jannieuwenhuizen"))
              (getenv "USER")))
(define guix-blessed "http://git.oban/~buildmaster/git/guix.git")
(define guix-public (string-append "http://git.oban/~" user "/guix.git"))

(define dezyne-blessed "http://git.oban/~buildmaster/git/development.git")
(define dezyne-public
  (string-append "http://192.168.32.106/~" user "/git/development.git"))

(define* (guix-job #:key
                   (url guix-blessed)
                   (branch "master"))
  `((#:name . ,(url->file-name url))
    (#:url . ,url)
    (#:branch . ,branch)
    (#:load-path . ".")
    (#:proc . hydra-jobs)
    (#:file . ,(local-file "gnu-system.scm"))
    (#:arguments (subset . "all") (name . "dezyne-server") (branch . ,branch))))

(define* (dezyne-job #:key
                     (url dezyne-blessed)
                     (branch "master")
                     (name "dezyne-server"))
  `((#:name . ,(string-append (url->file-name url) "-" name))
    (#:url . ,url)
    (#:branch . ,branch)
    (#:load-path . ,guix-checkout)
    (#:proc . guix-jobs)
    (#:file . ,(local-file "guix-track-git.scm"))
    (#:no-compile? . #t)
    (#:arguments (name . ,name) (branch . ,branch) (url . ,url))))

(list (guix-job #:url guix-checkout #:branch "npm")
      (dezyne-job #:url dezyne-public #:branch "development")
      (dezyne-job #:url dezyne-public #:branch "development" #:name "dezyne-server-release"))
